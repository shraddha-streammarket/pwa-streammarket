<?php

namespace Stream\Banner\Controller\Adminhtml\Index;

class Index extends \Magento\Backend\App\Action
{

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Stream_Banner::manager';


    protected $resultPageFactory = false;
    
    public function __construct(
            \Magento\Backend\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) 
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();

        $resultPage->setActiveMenu('Stream_Banner::manager')
            ->addBreadcrumb(__('Stream Manager'), __('Stream Manager'))
            ->addBreadcrumb(__('Manage Banner'), __('Manage Banner'));

        $resultPage->getConfig()->getTitle()->prepend((__('Manage Banner')));
        return $resultPage;
    }
    
    protected function _isAllowed()
    {
        return true;
    }
}

