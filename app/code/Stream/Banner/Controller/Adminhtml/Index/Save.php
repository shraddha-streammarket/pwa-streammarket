<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Stream\Banner\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Stream\Banner\Model\Banner;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Stream\Banner\Model\BannerFactory;

/**
 * Save Banner action.
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Stream_Banner::save';

    /**
     * @var BannerFactory
     */
    private $bannerFactory;

    /**
     * @var \Stream\Banner\Api\BannerRepositoryInterface
     */
    private $bannerRepository;

    /**
     * @param Action\Context $context
     * @param \Stream\Banner\Model\BannerFactory $bannerFactory
     * @param \Stream\Banner\Api\BannerRepositoryInterface $bannerRepository
     */
    public function __construct(
        Action\Context $context,
        BannerFactory $bannerFactory,
        \Stream\Banner\Api\BannerRepositoryInterface $bannerRepository
    ) {
        $this->bannerFactory = $bannerFactory;
        $this->bannerRepository = $bannerRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Stream\Banner\Api\BannerRepositoryInterface::class);
        //$this->banner = $bannerFactory;
        //$this->bannerRepository = $bannerRepository;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $data = $this->getRequest()->getPostValue();
            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = Banner::STATUS_ENABLED;
            }
            if (empty($data['entity_id'])) {
                $data['entity_id'] = null;
            }

            /** @var \Stream\Banner\Model\BannerFactory $model */
            $model = $this->bannerFactory->create();

            $id = $this->getRequest()->getParam('entity_id');
            if ($id) {
                try {
                    $model = $this->bannerRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This Banner no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            if (isset($data['image'][0]['name']) && isset($data['image'][0]['tmp_name'])) {

                $data['image'] =$data['image'][0]['name'];
                $this->imageUploader = \Magento\Framework\App\ObjectManager::getInstance()->get('Stream\Banner\ImageUpload');
                $this->imageUploader->saveFileToTmpDir('image');
                $this->imageUploader->moveFileFromTmp($data['image']);
            } elseif (isset($data['image'][0]['file']) && !isset($data['image'][0]['tmp_name'])) {
                $data['image'] = $data['image'][0]['file'];
            } else {
                $data['image'] = null;
            }

            $model->setData($data);

            try {
                $this->bannerRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the Banner.'));
                return $this->processResultRedirect($model, $resultRedirect, $data);
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Banner.'));
            }

            //$this->dataPersistor->set('stream_banner', $data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process result redirect
     *
     * @param \Stream\Banner\Data\BannerInterface $model
     * @param \Magento\Backend\Model\View\Result\Redirect $resultRedirect
     * @param array $data
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws LocalizedException
     */
    private function processResultRedirect($model, $resultRedirect, $data)
    {
        if ($this->getRequest()->getParam('back', false) === 'duplicate') {
            $newBanner = $this->bannerFactory->create(['data' => $data]);
            $newBanner->setId(null);
            $identifier = $model->getIdentifier() . '-' . uniqid();
            $newBanner->setIdentifier($identifier);
            $newBanner->setIsActive(false);
            $this->bannerRepository->save($newBanner);
            $this->messageManager->addSuccessMessage(__('You duplicated the Banner.'));
            return $resultRedirect->setPath(
                '*/*/edit',
                [
                    'entity_id' => $newBanner->getId(),
                    '_current' => true
                ]
            );
        }
        //$this->dataPersistor->clear('stream_banner');
        if ($this->getRequest()->getParam('back')) {
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getId(), '_current' => true]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
