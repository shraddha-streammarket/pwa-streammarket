<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stream\Banner\Model;

use Stream\Banner\Api\Data\BannerInterface;
use Stream\Banner\Api\BannerRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Stream\Banner\Model\BannerFactory;
use Stream\Banner\Model\ResourceModel\Banner\CollectionFactory as BannerCollectionFactory;
use Stream\Banner\Model\ResourceModel\Banner\Collection;
use Stream\Banner\Model\ResourceModel\Banner as ResourceBanner;
use Stream\Banner\Api\Data\BannerSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;


/**
 * Validates and saves a Banner
 */
class BannerRepository implements BannerRepositoryInterface
{

    /**
     * @var BannerFactory
     */
    private $bannerFactory;
 
    /**
     * @var BannerCollectionFactory
     */
    private $bannerCollectionFactory;

    /**
    *@var ResourceBanner
    */
    private $resource;

     /**
     * @var BannerSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    
    /**
     * @var SearchResultsInterface
     */
    private $searchResults;
 
 
    public function __construct(
        BannerFactory $bannerFactory,
        BannerCollectionFactory $bannerCollectionFactory,
        ResourceBanner $resource,
        BannerSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor,
        SearchResultsInterface $searchResults
    ) {
        $this->bannerFactory = $bannerFactory;
        $this->bannerCollectionFactory = $bannerCollectionFactory;
        $this->resource = $resource;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResults = $searchResults;
    }

    /**
     * @inheritdoc
     */
    public function save(BannerInterface $banner)
    {
        //print_r($banner->getData());exit;
        $storeIds = $this->fetchStoreIds($banner);
        $banner->setStoreId($storeIds);
        return $this->resource->save($banner);
    }

    public function fetchStoreIds($banner)
    {
        $storeIds = $banner->getStoreId();
        $storeIds = implode(',', $storeIds);
        return $storeIds;
    }

    /**
     * @inheritdoc
     */
    public function getById($bannerId)
    {
        $banner = $this->bannerFactory->create();
        $banner->getResource()->load($banner, $bannerId);
        if (! $banner->getId()) {
            throw new NoSuchEntityException(__('Unable to find banner with ID "%1"', $bannerId));
        }
        return $banner;
    }

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->bannerCollectionFactory->create();
 
        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);
 
        $collection->load();
        //print_r($collection->getData());exit;

        header("Content-Type: application/json; charset=utf-8");
        $this->response = json_encode($collection->getData());
        print_r($this->response,false);
        die();
        //return $this->buildSearchResult($searchCriteria, $collection);
    }

    private function addFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $fields[] = $filter->getField();
                if($filter->getConditionType()=='')
                    $filter->setConditionType('eq');
                $conditions[] = [$filter->getConditionType() => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
    }
 
    private function addSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ((array) $searchCriteria->getSortOrders() as $sortOrder) {
            $direction = $sortOrder->getDirection() == SortOrder::SORT_ASC ? 'asc' : 'desc';
            $collection->addOrder($sortOrder->getField(), $direction);
        }
    }
 
    private function addPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
    }
 
    private function buildSearchResult(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $searchResults = $this->searchResults;
 
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
 
        return $searchResults;
    }

    /**
     * @inheritdoc
     */
    public function delete(BannerInterface $banner)
    {
        try {
            $this->resource->delete($page);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __('Could not delete the page: %1', $exception->getMessage())
            );
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function deleteById($pageId)
    {
        return $this->delete($this->getById($pageId));
    }
}
