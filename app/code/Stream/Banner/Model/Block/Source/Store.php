<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Stream\Banner\Model\Block\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Store Options
 */
class Store implements OptionSourceInterface
{

    /**
     * All Store Views value
     */
    const ALL_STORE_VIEWS = '0';

    /**
    * Store
    **/
    private $storeManager;

    public function __construct(
    \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
         $this->_storeManager = $storeManager;
    }
    
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $storeManagerDataList = $this->_storeManager->getStores();
        $options = array();

        $options[] = [
            'label' => __('All Store Views'),
            'value' => self::ALL_STORE_VIEWS
        ];
        foreach ($storeManagerDataList as $key => $value) {
            $options[] = ['label' => $value['name'], 'value' => $key];
        }
        return $options;
    }
}
