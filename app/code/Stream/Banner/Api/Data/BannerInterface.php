<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Stream\Banner\Api\Data;

/**
 * CMS page interface.
 * @api
 * @since 100.0.2
 */
interface BannerInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID                = 'entity_id';
    const TITLE                    = 'title';
    const IS_ACTIVE                = 'is_active';
    const CREATION_TIME            = 'creation_time';
    const UPDATE_TIME              = 'update_time';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Stream\Banner\Api\Data\BannerInterface
     */
    public function setId($id);

    /**
     * Set title
     *
     * @param string $title
     * @return \Stream\Banner\Api\Data\BannerInterface
     */
    public function setTitle($title);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return \Magento\Cms\Api\Data\PageInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return \Magento\Cms\Api\Data\PageInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \Stream\Banner\Api\Data\BannerInterface
     */
    public function setIsActive($isActive);
}
