<?php

namespace Stream\Banner\Api;

interface BannerRepositoryInterface
{
    /**
     * Save Banner.
     *
     * @param \Stream\Banner\Api\Data\BannerInterface $banner
     * @return \Stream\Banner\Api\Data\BannerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Stream\Banner\Api\Data\BannerInterface $banner);

    /**
     * Retrieve banner.
     *
     * @param int $bannerId
     * @return \Stream\Banner\Api\Data\BannerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($bannerId);

    /**
     * Retrieve Banner matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Stream\Banner\Api\Data\BannerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Banner.
     *
     * @param \Stream\Banner\Api\Data\BannerInterface $banner
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Stream\Banner\Api\Data\BannerInterface $banner);

     /** 
     * Delete banner by ID.
     *
     * @param int $bannerId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($bannerId);
}