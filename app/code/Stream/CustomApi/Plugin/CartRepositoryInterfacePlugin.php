<?php
namespace Stream\CustomApi\Plugin;

use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;


use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Api\ProductRepositoryInterfaceFactory as ProductRepository;
use Magento\Catalog\Helper\ImageFactory as ProductImageHelper;
use Magento\Store\Model\StoreManagerInterface as StoreManager;
use Magento\Store\Model\App\Emulation as AppEmulation;
use Magento\Quote\Api\Data\CartItemExtensionFactory;
use Stream\CustomApi\Api\Data\OptionsInterfaceFactory;


/**
 * Class CartRepositoryInterfacePlugin
 */
class CartRepositoryInterfacePlugin
{

    /**
         * @var ObjectManagerInterface
         */
        protected $_objectManager;

        /**
         * @var ProductRepository
         */
        protected $productRepository;

        /**
         *@var \Magento\Catalog\Helper\ImageFactory
         */
        protected $productImageHelper;

        /**
         *@var \Magento\Store\Model\StoreManagerInterface
         */
        protected $storeManager;

        /**
         *@var \Magento\Store\Model\App\Emulation
         */
        protected $appEmulation;

        /**
         * @var CartItemExtensionFactory
         */
        protected $extensionFactory;

        /**
        * @var OptionsInterfaceFactory
        **/
        protected $optionsInterfaceFactory;

        /**
         * @var CurrencyInterface
         */
        private $localeCurrency;

        /**
         * @param \Magento\Framework\ObjectManagerInterface $objectManager
         * @param ProductRepository $productRepository
         * @param \Magento\Catalog\Helper\ImageFactory
         * @param \Magento\Store\Model\StoreManagerInterface
         * @param \Magento\Store\Model\App\Emulation
         * @param CartItemExtensionFactory $extensionFactory
         * @param OptionsInterfaceFactory $optionsInterfaceFactory
         * @param CurrencyInterface $localeCurrency
        */
        public function __construct(
            \Magento\Framework\ObjectManagerInterface $objectManager,
            ProductRepository $productRepository,
            ProductImageHelper $productImageHelper,
            StoreManager $storeManager,
            AppEmulation $appEmulation,
            CartItemExtensionFactory $extensionFactory,
            OptionsInterfaceFactory $optionsInterfaceFactory,
            \Magento\Framework\Locale\CurrencyInterface $localeCurrency
        ) {
            $this->_objectManager = $objectManager;
            $this->productRepository = $productRepository;
            $this->productImageHelper = $productImageHelper;
            $this->storeManager = $storeManager;
            $this->appEmulation = $appEmulation;
            $this->extensionFactory = $extensionFactory;
            $this->optionsInterfaceFactory = $optionsInterfaceFactory;
            $this->localecurrency = $localeCurrency;
        }
     /**
     * Enables an administrative user to return information for a specified cart.
     *
     * @param CartRepositoryInterface $subject
     * @param CartInterface $cart
     * @return CartInterface
     */
    public function afterget(CartRepositoryInterface $subject,CartInterface $cart)
    {

        // get cart items
        $items = $cart->getItems();        

       /**
         * Code to add the extension_attributes
         */
        foreach ($items as $item) {
            

            $itemExtAttr = $item->getExtensionAttributes();
            if ($itemExtAttr === null) {
                $itemExtAttr = $this->extensionFactory->create();
            }

            if($item->getProductId())
            {
                $product = $this->productRepository->create()->getById($item->getProductId());

                //set item image
                $imageurl =$this->productImageHelper->create()->init($product, 'image')->setImageFile($product->getThumbnail())->getUrl();
                $itemExtAttr->setImageUrl($imageurl);

                //set selected options
                $options = array();
                $_AllOptions = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct()); 
                //print_r($_AllOptions);exit;
                if(array_key_exists('options', $_AllOptions)){
                    $selectedItemOptions = [];
                    foreach ($_AllOptions['options'] as $option) {
                        $itemOptions = $this->optionsInterfaceFactory->create();
                        $itemOptions->setLabel($option['label']);
                        $itemOptions->setValue($option['value']);
                        $itemOptions->setOptionId($option['option_id']);
                        $itemOptions->setOptionType($option['option_type']);
                        $itemOptions->setOptionValue($option['option_value']);

                        $selectedItemOptions[] = $itemOptions;
                        
                    }

                    $itemExtAttr->setOptions($selectedItemOptions);

                    
                }


                if(array_key_exists('attributes_info', $_AllOptions)){
                    $selectedItemOptions = [];
                    foreach ($_AllOptions['attributes_info'] as $option) {
                        $itemOptions = $this->optionsInterfaceFactory->create();

                        $itemOptions->setLabel($option['label']);
                        $itemOptions->setValue($option['value']);
                        $itemOptions->setOptionId($option['option_id']);
                        $itemOptions->setOptionValue($option['option_value']);

                        $selectedItemOptions[] = $itemOptions;
                    }
                    $itemExtAttr->setOptions($selectedItemOptions);
                    
                }

                //set Parent product sku
                //echo $item->getParentItemId();exit;
                // if($parent = $item->getParentItem()){  
                //     $parent->getProduct()->getSku();
                //     $parentSku = $parent->getSku();

                //     $itemExtAttr->setParentSku($parentSku);
                // }

                $itemExtAttr->setParentSku($product->getSku());

                //set Currency Symbol
                $currencycode = $cart->getCurrency()->getQuoteCurrencyCode();
                $currencySymbol = $this->getCurrencySymbol($currencycode);
                $itemExtAttr->setCurrencySymbol($currencySymbol);

            }
               

            // uncomment in future for other type of products
            // $this->getSelectedOptions($item);
            


            $item->setExtensionAttributes($itemExtAttr);
        }


        return $cart;
    }

    //return currency symbol
    public function getCurrencySymbol($currencycode)
    {
        return $this->localecurrency->getCurrency($currencycode)->getSymbol();
    }

    /**
     * Helper function that provides full cache image url
     * @param \Magento\Catalog\Model\Product
     * @return string
     */
    protected function getImageUrl($product, string $imageType = NULL)
    {
        $storeId = $this->storeManager->getStore()->getId();

        $this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);
        $imageUrl = $this->productImageHelper->create()->init($product, $imageType)->getUrl();

        $this->appEmulation->stopEnvironmentEmulation();

        return $imageUrl;
    }


    /*
     * get Configurable/Bundle selected options from Item object
     */
    public function getSelectedOptions($item){
        $result = [];
        $options = $item->getProductOptions();
        if ($options) {
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (isset($options['attributes_info'])) {
                $result = array_merge($result, $options['attributes_info']);
            }
        }
        return $result;
    }

}