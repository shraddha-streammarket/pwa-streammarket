<?php
namespace Stream\CustomApi\Plugin;

use Magento\Quote\Api\CartTotalRepositoryInterface;
use Magento\Quote\Api\Data\TotalsInterface;

use Magento\Framework\Api\ExtensionAttributesFactory;


/**
 * Class CartRepositoryInterfacePlugin
 */
class CartTotalRepositoryInterfacePlugin
{

    /**
     * @var ExtensionAttributesFactory
     */
    private $extensionFactory;

    /**
     * @var CurrencyInterface
     */
    private $localeCurrency;

    /**
     * Initialize dependencies.
     *
     * @param ExtensionAttributesFactory $extensionFactory
     * @param CurrencyInterface $localeCurrency
     */
    public function __construct(
        ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Locale\CurrencyInterface $localeCurrency
    ) {
        $this->extensionFactory = $extensionFactory;
        $this->localecurrency = $localeCurrency;
    }

     /**
     * Enables an administrative user to return information for a specified cart.
     *
     * @param CartTotalRepositoryInterface $subject
     * @param TotalsInterface $cartTotal
     * @return TotalsInterface
     */
    public function afterget(CartTotalRepositoryInterface $subject,TotalsInterface $cartTotal)
    {

        $extensionAttributes = $cartTotal->getExtensionAttributes();

        if ($extensionAttributes === null) {
            $extensionAttributes = $this->extensionFactory->create(TotalsInterface::class);
            $cartTotal->setExtensionAttributes($extensionAttributes);
        }

        $currencycode = $cartTotal->getQuoteCurrencyCode();

        $currencySymbol = $this->getCurrencySymbol($currencycode);


        $extensionAttributes->setCurrencySymbol($currencySymbol);

        $cartTotal->setExtensionAttributes($extensionAttributes);

        return $cartTotal;
    }

    //return currency symbol
    public function getCurrencySymbol($currencycode)
    {
        return $this->localecurrency->getCurrency($currencycode)->getSymbol();
    }

}