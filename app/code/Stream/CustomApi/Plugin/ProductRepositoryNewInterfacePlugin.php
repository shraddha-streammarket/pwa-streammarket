<?php
namespace Stream\CustomApi\Plugin;

use Magento\Catalog\Api\ProductRepositoryInterface as ProductRepository;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Catalog\Api\Data\ProductInterface;
use \Magento\Framework\Api\SearchResults;

/**
 * Class ProductRepositoryInterfacePlugin
 */
class ProductRepositoryNewInterfacePlugin
{
    /**
     * @var ExtensionAttributesFactory
     */
    private $extensionFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CurrencyFactory
     */
    private $currencyFactory;

    /**
     * @var RatingFactory
     */
    protected $_ratingFactory;

    /**
     * @var Rating CollectionFactory
     */
    protected $_reviewFactory;

    /**
     * @var configurable type model $_configModel
     */
    protected $_configModel;

    /**
     * Initialize dependencies.
     *
     * @param SubscriberFactory $subscriberFactory
     * @param ExtensionAttributesFactory $extensionFactory
     * @param Subscriber $subscriberResource
     */
    public function __construct(
        ExtensionAttributesFactory $extensionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Review\Model\RatingFactory $ratingFactory,
        \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewFactory,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configModel
    ) {
        $this->extensionFactory = $extensionFactory;
        $this->storeManager = $storeManager;
        $this->currencyFactory = $currencyFactory;
        $this->product = $productFactory->create();

        $this->_ratingFactory = $ratingFactory;
        $this->_reviewFactory = $reviewFactory;
        $this->_configModel = $configModel;
    }


    /**
     * Add extension attribute to product data object to make it accessible in API data
     *
     * @param ProductRepository $subject
     * @param ProductInterface $product
     *
     * @return ProductInterface
     */
    public function afterGetById(ProductRepository $subject, ProductInterface $product)
    {
        
        $extensionAttributes = $product->getExtensionAttributes();

        if ($extensionAttributes === null) {
            $extensionAttributes = $this->extensionFactory->create(ProductInterface::class);
            $product->setExtensionAttributes($extensionAttributes);
        }

        $finalPrice = $product->getFinalPrice();
        $extensionAttributes->setFinalPrice($finalPrice+0);
        $product->setExtensionAttributes($extensionAttributes);
        return $product;
    }
    /**
     * Add extension attribute to product data object to make it accessible in API data
     *
     * @param ProductRepository $subject
     * @param ProductInterface $product
     *
     * @return ProductInterface
     */
    public function afterGet(ProductRepository $subject, ProductInterface $product)
    {

        $finalPrice = $product->getFinalPrice();

        if($product->getTypeId()=='bundle' || $product->getTypeId()=='grouped')
        {
            $finalPriceRange = $this->getPrice($product->getId());

            $finalPrice = $finalPriceRange;
        }
        $extensionAttributes = $product->getExtensionAttributes();

        if ($extensionAttributes === null) {
            $extensionAttributes = $this->extensionFactory->create(ProductInterface::class);
            $product->setExtensionAttributes($extensionAttributes);
        }

        $extensionAttributes->setFinalPrice($finalPrice+0);

        $currencyCode = $this->storeManager->getStore()->getCurrentCurrencyCode(); 
        $currency = $this->currencyFactory->create()->load($currencyCode); 
        $currencySymbol = $currency->getCurrencySymbol();

        $extensionAttributes->setCurrencySymbol($currencySymbol);

        /**Add Review and rating**/
        //echo $product->getId();
        $reviews = $this->getReviewCollection($product->getId());
        //$reviews = json_encode($reviews);
        $extensionAttributes->setProReview($reviews);


        if($product->getTypeId()=='configurable')
        {
            $productAttributeOptions = $this->_configModel->getConfigurableAttributesAsArray($product);
            $extensionAttributes->setOptionsData($productAttributeOptions);
        }


        $product->setExtensionAttributes($extensionAttributes);
        return $product;
    }

    /**
     * Add extension attribute to product data object to make it accessible in API data
     *
     * @param ProductRepository $subject
     * @param SearchResults $searchCriteria
     *
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function afterGetList(
        ProductRepository $subject,
        SearchResults $searchCriteria
    )
    {
        $products = [];
        foreach ($searchCriteria->getItems() as $entity) {
            $finalPrice = $entity->getFinalPrice();

            if($entity->getTypeId()=='bundle' || $entity->getTypeId()=='grouped')
            {
                $finalPriceRange = $this->getPrice($entity->getId());

                $finalPrice = $finalPriceRange;
            }

            $extensionAttributes = $entity->getExtensionAttributes();
            $extensionAttributes->setFinalPrice($finalPrice+0);
            

            $currencyCode = $this->storeManager->getStore()->getCurrentCurrencyCode(); 
            $currency = $this->currencyFactory->create()->load($currencyCode); 
            $currencySymbol = $currency->getCurrencySymbol();

            $extensionAttributes->setCurrencySymbol($currencySymbol);            

            
            $entity->setExtensionAttributes($extensionAttributes);

            $products[] = $entity;
        }

       

        $searchCriteria->setItems($products);
        return $searchCriteria;
    }

    public function getPrice($productId)
    {
        $productObj = $this->product->load($productId);

        $finalPrice   = $productObj->getPriceInfo()->getPrice('final_price')->getValue();
        $minimumPrice = $productObj->getPriceInfo()->getPrice('final_price')->getMinimalPrice()->getValue();
        $maximumPrice = $productObj->getPriceInfo()->getPrice('final_price')->getMaximalPrice()->getValue();

        $return['minimumPrice'] = $minimumPrice;
        $return['maximumPrice'] = $maximumPrice;

        return $return;
    }

    public function getReviewCollection($productId){
        $collection = $this->_reviewFactory->create()
        ->addStatusFilter(
            \Magento\Review\Model\Review::STATUS_APPROVED
        )->addEntityFilter(
            'product',
            $productId
        )->setDateOrder();
        return $collection->getData();
    }

    public function getRatingCollection(){
        $ratingCollection = $this->_ratingFactory->create()
        ->getResourceCollection()
        ->addEntityFilter(
            'product' 
        )->setPositionOrder()->setStoreFilter(
            $this->storeManager->getStore()->getId()
        )->addRatingPerStoreName(
            $this->storeManager->getStore()->getId()
        )->load();

        return $ratingCollection->getData();
    }
}