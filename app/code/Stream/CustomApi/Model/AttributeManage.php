<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stream\CustomApi\Model;


class AttributeManage implements \Stream\CustomApi\Api\AttributeManageInterface
{
    private $eavModel;

    private $entityAttribute;

    /**
    * @param  \Magento\Catalog\Model\ResourceModel\Eav\Attribute $eavModel
    * @param \Magento\Eav\Model\Entity\Attribute $entityAttribute
    **/
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $eavModel,
        \Magento\Eav\Model\Entity\Attribute $entityAttribute
    ) 
    {
        $this->_eavModel = $eavModel;
        $this->_entityAttribute = $entityAttribute;
    }

    /**
     * {@inheritdoc}
     */
    public function fetchAttributeData($attributeId)
    {
        //$eavModel = $this->_objectManager->create('Magento\Catalog\Model\ResourceModel\Eav\Attribute');
        $attr = $this->_eavModel->load($attributeId);
        $attributeCode = $attr->getAttributeCode();//Get attribute code from its id
        //echo $attributeCode;

         /*Get attribute details*/
        $attributeDetails = $this->getAttributeInfo("catalog_product", $attributeCode);
        $data = $attributeDetails->getData();
        $data = json_encode($data);
        echo $data; 
        exit;
    }


    public function getAttributeInfo($entityType, $attributeCode)
    {
        return $this->_entityAttribute
                    ->loadByCode($entityType, $attributeCode);
    }
 
}
