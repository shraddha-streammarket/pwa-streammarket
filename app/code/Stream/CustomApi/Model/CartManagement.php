<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stream\CustomApi\Model;

use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Api\CartRepositoryInterface;

/**
 * Cart Management class
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CartManagement implements \Stream\CustomApi\Api\CartManagementInterface
{

    /**
     * @var CartManagementInterface
     */
    protected $quoteManagement;

    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * Initialize dependencies.
     *
     * @param CartManagementInterface $quoteManagement
     * @param CartRepositoryInterface $cartRepository
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        CartManagementInterface $quoteManagement,
        CartRepositoryInterface $cartRepository
    ) {
        $this->quoteManagement = $quoteManagement;
        $this->cartRepository = $cartRepository;
    }
    /**
     * {@inheritdoc}
     */
    public function placeOrder($cartId, PaymentInterface $paymentMethod = null)
    {
        /** @var $quoteIdMask QuoteIdMask */
        //$quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        $this->cartRepository->get($cartId)
            ->setCheckoutMethod(CartManagementInterface::METHOD_GUEST);
        return $this->quoteManagement->placeOrder($cartId, $paymentMethod);
    }
}
