<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stream\CustomApi\Model;

use Magento\Quote\Api\Data\CartItemInterface;
use Magento\Quote\Api\CartItemRepositoryInterface;
use Magento\Quote\Model\QuoteIdMask;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Quote\Api\CartManagementInterface;

/**
 * Cart Management.
 */
class ProductCartItemReprository implements \Stream\CustomApi\Api\ProductCartItemReprositoryInterface
{


    /**
     * @var \Magento\Quote\Api\CartItemRepositoryInterface
     */
    protected $repository;

    /**
     * @var QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * @var CartManagementInterface
     */
    protected $quoteManagement;

    /**
     * Constructs a read service object.
     *
     * @param \Magento\Quote\Api\CartItemRepositoryInterface $repository
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     */
    public function __construct(
        \Magento\Quote\Api\CartItemRepositoryInterface $repository,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        CartManagementInterface $quoteManagement
    ) {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->repository = $repository;
        $this->quoteManagement = $quoteManagement;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Magento\Quote\Api\Data\CartItemInterface $cartItem)
    {
        /** @var $quoteIdMask QuoteIdMask */
        if(!empty($cartItem->getQuoteId()))
        {
            $cartItem->setQuoteId($cartItem->getQuoteId());
        }
        else {
            $maskedId = $this->createEmptyCart();
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($maskedId, 'masked_id');
            $cartItem->setQuoteId($quoteIdMask->getQuoteId());
        }
        
        return $this->repository->save($cartItem);
    }

    public function createEmptyCart()
    {
        /** @var $quoteIdMask \Magento\Quote\Model\QuoteIdMask */
        $quoteIdMask = $this->quoteIdMaskFactory->create();
        $cartId = $this->quoteManagement->createEmptyCart();
        $quoteIdMask->setQuoteId($cartId)->save();
        return $quoteIdMask->getMaskedId();
    }


    /**
     * {@inheritdoc}
     */
    public function updateItems($cartItems)
    {
        $return = array();
        foreach($cartItems as $cartItem)
        {
            /** @var $quoteIdMask QuoteIdMask */
            if(!empty($cartItem->getQuoteId()))
            {
                $cartItem->setQuoteId($cartItem->getQuoteId());
                $cartItem->setItemId($cartItem->getItemId());
                $cartItem->setQty($cartItem->getQty());
            }
            else {
                $maskedId = $this->createEmptyCart();
                $quoteIdMask = $this->quoteIdMaskFactory->create()->load($maskedId, 'masked_id');
                $cartItem->setQuoteId($quoteIdMask->getQuoteId());                
            }

            $return[] = $this->repository->save($cartItem);
        }
        
        
        return $return;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($cartId, $itemId)
    {
        /** @var $quoteIdMask QuoteIdMask */
        return $this->repository->deleteById($cartId, $itemId);
    }

}
