<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stream\CustomApi\Model;

use Magento\Catalog\Model\Product as ModelProduct;
use Magento\Store\Model\Store;
use Magento\Swatches\Helper\Data;

/**
 * Repository for categories.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class LayeredNavigationData implements \Stream\CustomApi\Api\LayeredNavigationDataInterface
{


    protected $_request;
    protected $_filterableAttributeList;
    protected $_layerResolver;
    protected $_filterList;
    protected $_storeManagerInterface;
    protected $_response;
    protected $_redirFactory;
    protected $_swatchesHelper;

    public function __construct(
        \Magento\CatalogGraphQl\Model\Resolver\Layer\FilterableAttributesListFactory $filterableAttributeList,
        \Magento\Catalog\Model\Layer\FilterListFactory $filterList,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\Framework\Webapi\Rest\Request $request,
        Data $swatchesHelper
    )
    {
        $this->_filterList                      = $filterList;
        $this->_filterableAttributeList         = $filterableAttributeList;
        $this->_layerResolver                   = $layerResolver;
        $this->_request                         = $request;
        $this->_storeManagerInterface           = $storeManagerInterface;
        $this->_swatchesHelper                  = $swatchesHelper;
    }

    /**
     * @inheritdoc
    */
    public function getFilterList($categoryId)
    {
    //$categoryId   = $this->_request->getParam('categoryId');
        $layer        = $this->_layerResolver->get();

        $layerType = "search";
        if ($categoryId){
            $layer->setCurrentCategory($categoryId);
            $layerType = "category";
        }

        $collection = $layer->getProductCollection();
        //$collection->addAttributeToFilter('visibility',array('nteq' => 1));

        $params = $this->_request->getParams();

        

        $appliedFiltersCode = [];
        $appliedFiltersValues = [];
        if(!empty($params['filter']))
        {
            $paramFilter = $params['filter'];
            foreach($paramFilter as $key=>$value)
            {
                $collection->addAttributeToFilter($key,array($value['condition'] => $value['value']));
                $appliedFiltersCode[] = $key;
                $appliedFiltersValues[] = $value['value'];
            }
        }

        // $collection->getSelect()->joinLeft(array('link_table' => 'catalog_product_super_link'),'link_table.parent_id = e.entity_id',
        //     array('product_id','parent_id')
        // );

        // $collection->getSelect()->group('e.entity_id');
        
        // print_r($collection->getData());
        // exit;
        // $collection = $layer->getProductCollection()
        //                 ->addAttributeToFilter('fashion_color',array('in' => 5641));

        $filterArray['store_id']  = $this->_storeManagerInterface->getStore()->getId();
        $filterableAttributesList = $this->_filterableAttributeList->create($layerType);

        $filterList = $this->_filterList->create(['filterableAttributes' => $filterableAttributesList]);
        $filters    = $filterList->getFilters($layer);
        $i = 0;

        foreach($filters as $filter)
        {
            // Don't show options with no items
            if (! $filter->getItemsCount()) {continue;}
            
            $availablefilter = (string)$filter->getName();
            $items           = $filter->getItems();
            $filterValues    = array();

            $j = 0;

            
            
            foreach($items as $item)
            {
                $hide = 0;
                if($collection->getSize()<=$item->getCount())
                {
                   if(in_array($filter->getRequestVar(),$appliedFiltersCode) && in_array($item->getValue(),$appliedFiltersValues)){
                        $hide = 1;
                    }
                    
                }
                
                if(!in_array($filter->getRequestVar(),$appliedFiltersCode) && !in_array($item->getValue(),$appliedFiltersValues)){
                    $filterValues[$j]['display'] = htmlspecialchars_decode(strip_tags($item->getLabel()));
                    $filterValues[$j]['value']   = $item->getValue();
                    $filterValues[$j]['count']   = $item->getCount(); //Gives no. of products in each filter options
                    //$filterValues[$j]['hide']   = $collection->count();

                    
                    $filterValues[$j]['image-hashcode']   = $this->getAtributeSwatchHashcode($item->getValue());
                    //$filterValues[$j]['url']     = $item->getUrl();   //Gives filter url.
                    $filterValues[$j]['code'] = $filter->getRequestVar();
                }
                $j++;
            }

            if(!empty($filterValues) && count($filterValues)>1)
            {
                $filterArray['availablefilter'][$availablefilter] =  $filterValues;
            }
            else if(!empty($filterValues) && count($filterValues)==1)
            {
                if($hide==0)
                {
                    $filterArray['availablefilter'][$availablefilter] =  $filterValues;
                }
                else
                {
                    $filterArray['availablefilter'][$availablefilter] = '';
                }
            }
            $i++;

        }

        if (!isset($filterArray["availablefilter"])) {
            $filterArray['availablefilter'] = "No filters to show.";
        }

        header("Content-Type: application/json; charset=utf-8");
        $this->response = json_encode($filterArray);
        print_r($this->response,false);
        die();
    }

    /* Get Hashcode of Visual swatch by option id */
    public function getAtributeSwatchHashcode($optionid) {
        $hashcodeData = $this->_swatchesHelper->getSwatchesByOptionsId([$optionid]);
        if(!empty($hashcodeData[$optionid]))
            return $hashcodeData[$optionid]['value'];
        else
            return '';
    }

}
