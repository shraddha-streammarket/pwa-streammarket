<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Stream\CustomApi\Model;

class FetchRegion implements \Stream\CustomApi\Api\FetchRegionInterface
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Directory\Model\Country $country,
        array $data = []
    ) {
        $this->country = $country;
    }
 
    /**
    * {@inheritDoc}
    */
    public function getRegionsOfCountry($countryCode) {
        $regionCollection = $this->country->loadByCode($countryCode)->getRegions();
        $regions = $regionCollection->loadData()->toOptionArray(false);
        return $regions;
    }
}
