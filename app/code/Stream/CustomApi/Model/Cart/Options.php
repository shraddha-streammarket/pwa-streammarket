<?php
namespace Stream\CustomApi\Model\Cart;

use Stream\CustomApi\Api\Data\OptionsInterface;

class Options extends \Magento\Framework\Model\AbstractExtensibleModel implements OptionsInterface
{
    /**
     * @inheritDoc
     */
    public function getLabel()
    {
        return $this->getData('label');
    }

    /**
     * @inheritDoc
     */
    public function setLabel($value)
    {
        $this->setData('label', $value);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getValue()
    {
        return $this->getData('value');
    }

    /**
     * @inheritDoc
     */
    public function setValue($value)
    {
        $this->setData('value', $value);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOptionId()
    {
        return $this->getData('option_id');
    }

    /**
     * @inheritDoc
     */
    public function setOptionId($value)
    {
        $this->setData('option_id', $value);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOptionType()
    {
        return $this->getData('option_type');
    }

    /**
     * @inheritDoc
     */
    public function setOptionType($value)
    {
        $this->setData('option_type', $value);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOptionValue()
    {
        return $this->getData('option_value');
    }

    /**
     * @inheritDoc
     */
    public function setOptionValue($value)
    {
        $this->setData('option_value', $value);
        return $this;
    }
}