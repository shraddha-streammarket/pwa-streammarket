<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stream\CustomApi\Api;

/**
 * @api
 * @since 100.0.2
 */
interface CategoryFetchInterface
{

    /**
     * @inheritdoc
     *
     * @param int $rootCategoryId
     * @param int $depth
     * @return \Magento\Catalog\Api\Data\CategoryTreeInterface
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTree($categoryId = null, $depth = null);

}
