<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stream\CustomApi\Api;

use Magento\Quote\Api\Data\PaymentInterface;

/**
 * @api
 * @since 100.0.2
 */
interface CartManagementInterface
{

    /**
     * Place an order for a specified cart.
     *
     * @param int $cartId The cart ID.
     * @param PaymentInterface|null $paymentMethod
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return int Order ID.
     */
    public function placeOrder($cartId, PaymentInterface $paymentMethod = null);

}
