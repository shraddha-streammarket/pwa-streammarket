<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stream\CustomApi\Api;

/**
 * @api
 * @since 100.0.2
 */
interface FetchRegionInterface
{

    /**
     * Return quote totals data for a specified cart.
     *
     * @param string $countryCode The cart ID.
     * @return string[]|Void
     */
    public function getRegionsOfCountry($countryCode);

}
