<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stream\CustomApi\Api;

/**
 * @api
 * @since 100.0.2
 */
interface LayeredNavigationDataInterface
{

    /**
     * @api
     * @param int $categoryId
     * @return array
     */
    public function getFilterList($categoryId);

}
