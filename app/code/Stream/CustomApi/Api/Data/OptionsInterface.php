<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stream\CustomApi\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface OptionsInterface
{
    /**
     * @return string
     */
    public function getLabel();

    /**
     * @param string $value
     * @return $this
     */
    public function setLabel($value);

    /**
     * @return string
     */
    public function getValue();

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value);

    /**
     * @param int $value
     * @return $this
     */
    public function getOptionId();

    /**
     * @param int $value
     * @return $this
     */
    public function setOptionId($value);

   /**
     * @param string $value
     * @return $this
     */
    public function getOptionType();

    /**
     * @param string $value
     * @return $this
     */
    public function setOptionType($value);

    /**
     * @param string $value
     * @return $this
     */
    public function getOptionValue();

    /**
     * @param string $value
     * @return $this
     */
    public function setOptionValue($value);
}
