<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Stream\CustomApi\Api;

/**
 * @api
 * @since 100.0.2
 */
interface AttributeManageInterface
{

    /**
     * Return quote totals data for a specified cart.
     *
     * @param int $attributeId
     * @return mixed
     */
    public function fetchAttributeData($attributeId);

}
